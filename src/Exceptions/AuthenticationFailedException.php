<?php

namespace Lihui\IsmsWrapper\Exceptions;

use Exception;

class AuthenticationFailedException extends Exception {
  public function __construct() {
    parent::__construct('AUTHENTICATION FAILED. Your username or password are incorrect.', -1001);
  }
}