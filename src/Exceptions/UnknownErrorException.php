<?php

namespace Lihui\IsmsWrapper\Exceptions;

use Exception;

class UnknownErrorException extends Exception {
  public function __construct() {
    parent::__construct('UNKNOWN ERROR. Please contact the administrator.', -1000);
  }
}