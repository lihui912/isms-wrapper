<?php

namespace Lihui\IsmsWrapper;

use Exception;

use Lihui\IsmsWrapper\Exceptions\KeyNotExistsException;

class Config {
  
  private $config = [];
  
  public function __construct(){
    $this->config['un'] = '';
    $this->config['pwd'] = '';
  }
  
  public function __set(string $name = '', $value = null) {
    if(true === empty($name)) {
      throw new Exception('Empty config name.');
    }
    
    if(false === $this->hasKey($name)) {
      throw new KeyNotExistsException();
    }
    
    $this->config[$name] = $value;
  }
  
  public function __get(string $name = '') {
    if(true === empty($name)) {
      throw new Exception('Empty config name.');
    }
    
    if(false === $this->hasKey($name)) {
      throw new KeyNotExistsException();
    }
    
    return $this->config[$name];
  }
  
  private function hasKey(string $name = '') {
    return array_key_exists($name, $this->config);
  }
}