<?php

namespace Lihui\IsmsWrapper;

use Exception;

use Lihui\IsmsWrapper\Exceptions\EmptyStringException;

class Message {
  const TYPE_ASCII = '1';
  const TYPE_UNICODE = '2';
  
  private $recipientList = [];
  private $message = '';
  
  public function __construct() {
    
  }
  
  public function setRecipient(string $phoneNumber = '') {
    $phoneNumber = trim($phoneNumber);
    
    if(true === empty($phoneNumber)) {
      throw new EmptyStringException();
    }
    
    $this->$recipientList[] = $phoneNumber;
  }
  
  public function getRecipientList() {
    return array_unique($this->recipientList);
  }
  
  public function setMessage(string $message = '') {
    $message = trim($message);
    
    if(true === empty($message)) {
      throw new EmptyStringException();
    }
    
    $this->message = $message;
  }
  
  public function getMessage() {
    return $this->message;
  }
}