<?php

namespace Lihui\IsmsWrapper;

class Error {
  const E_SUCCESS                     = '2000';
  const E_UNKNOWN_ERROR               = '-1000';
  const E_AUTHENTICATION_FAILED       = '-1001';
  const E_ACCOUNT_SUSPENDED_EXPIRED   = '-1002';
  const E_IP_NOT_ALLOWED              = '-1003';
  const E_INSUFFICIENT_CREDITS        = '-1004';
  const E_INVALID_SMS_TYPE            = '-1005';
  const E_INVALID_BODY_LENGTH         = '-1006';
  const E_INVALID_HEX_BODY            = '-1007';
  const MISSING_PARAMETER             = '-1008';
  const E_INVALID_DESTINATION_NUMBER  = '-1009';
  const E_INVALID_MESSAGE_TYPE        = '-1012';
  const E_INVALID_TERM_AND_AGREEMENT  = '-1013';
}