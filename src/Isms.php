<?php

namespace Lihui\IsmsWrapper;

use DateTime;

use Lihui\IsmsWrapper\Config;
use Lihui\IsmsWrapper\Message;

use Lihui\IsmsWrapper\Exceptions\EmptyMessageObjectException;
use Lihui\IsmsWrapper\Exceptions\EmptyDataException;

use Curl\Curl;

class Isms {
  const HOST = ['https://www.isms.com.my/', 'https://www.vocotext.com/'];
  const URL_SEND = 'isms_send.php';
  const URL_BALANCE_CHECK = 'isms_balance.php';
  const URL_ACCOUNT_EXPIRY = 'isms_expiry_date.php';
  
  private $config = null;
  
  public function __construct(Config $config = null) {
    if(true === is_null($config)) {
      $this->config = new Config();
    } else {
      $this->config = $config;
    }
  }
  
  public function checkBalance() {
    $type = Isms::URL_BALANCE_CHECK;
    $url = Isms::HOST[rand(0, count(Isms::HOST) - 1)] . $type;
    $response = $this->execCurl($url);
    
    $response->close();
    
    return $response->response;
  }
  
  public function checkExpireDate() {
    $type = Isms::URL_ACCOUNT_EXPIRY;
    $url = Isms::HOST[rand(0, count(Isms::HOST) - 1)] . $type;
    $response = $this->execCurl($url);
    $response->close();
    
    return $parsedData;
  }
  
  private function execCurl(string $url = '', array $curlData = []) {
    if(true === empty($url)) {
      throw new EmptyStringException('Empty URL.');
    }
    
    $curlData['un'] = $this->config->un;
    $curlData['pwd'] = $this->config->pwd;
    
    $curl = new Curl();
    $curl->setOpt(CURLOPT_RETURNTRANSFER, true);
    $curl->setOpt(CURLOPT_SSL_VERIFYPEER, false);
    
    return $curl->get($url, $curlData);
  }
  
}